SMP=${2-2}

kvm -smp $SMP -m 2048 -boot d \
    -cdrom debian-10.10.0-amd64-netinst.iso \
    -hda debian.qcow \
    -net nic -net user,hostfwd=tcp::10022-:22 \
    -serial stdio

