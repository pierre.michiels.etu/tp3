
# Table of Contents

1.  [Documents](#orged08d3a)
    1.  [Task structure](#org0b9c918)
    2.  [Blocking Processes and threads](#org9ba9fde)
    3.  [Mutexes](#org7420e40)
    4.  [Spinlocks and atomic operations](#org21254c9)
2.  [TP 3](#org657695e)



<a id="orged08d3a"></a>

# Documents

[Book](https://sysprog21.github.io/lkmpg/) 


<a id="org0b9c918"></a>

## Task structure

Inside the kernel, a thread (or a process) is called a task. All
information about a task are contained in the [task structure](https://elixir.bootlin.com/linux/latest/source/include/linux/sched.h#L661).

In this task structure there are many relevant things: 

-   the process id (pid)
-   its exit state (in case the task is in zombie state)
-   the amount of time it has executed (the utime/stime/gtime fields)
-   the scheduling policy for this task
-   its priority

and so many other things. One of the most important is the task
`state`. A task can be in one of the [following states](https://elixir.bootlin.com/linux/latest/source/include/linux/sched.h#L82): 

-   `TASK_RUNNING`, the task is executing
-   `TASK_INTERRUPTIBLE`, the process sleeps waiting for an event or a signal
-   `TASK_UNINTERRUPTIBLE`, the process waits for something, but it
    cannot be wake-up by a signal
-   `TASK_STOPPED`, the task waits for a `SIG_CONTINUE`
-   `TASK_TRACED`, the task has been suspended by a debugger
-   `EXIT_ZOMBIE` and `EXIT_DEAD`, the task has finished executing,
    but the structure has not been deleted yet.


<a id="org9ba9fde"></a>

## Blocking Processes and threads

When a thread needs to wait for an event, it can be blocked (put to
sleep) by changing its status and inserting it in a waiting queue.
See `modules/sleepmod.c` and the description [here](https://sysprog21.github.io/lkmpg/#sleep).

(Warning, the code shown on the book has a bug, use
the one in this repository instead).

(For the braves: try to understand where the bug is, and why it
happened).


<a id="org7420e40"></a>

## Mutexes

Kernel mutexes are very similar to userland mutexes. You can use
them almost in the same way. See [here](https://elixir.bootlin.com/linux/latest/source/include/linux/mutex.h#L54) for the definition. 

In particular, `mutex_lock()` tries to lock the mutex, and if it
fails, it blocks on a queue (i.e. its state becomes
`TASK_UNINTERRUPTIBLE`, see [here](https://elixir.bootlin.com/linux/latest/source/kernel/locking/mutex.c#L1358)).

Mutex operations are easy to use but are internally complex, see
for example the code of the most common case [here](https://elixir.bootlin.com/linux/latest/source/kernel/locking/mutex.c#L926). Therefore, use
them when you have no stringent performance requirement.

See the example [here](https://sysprog21.github.io/lkmpg/#mutex). 


<a id="org21254c9"></a>

## Spinlocks and atomic operations

In case your critical section is very short (a few tenths of
instructions), you may consider using a lower level mechanism
called *spinlock*. A spinlock is a busy-wait on a condition, that
is the task does not sleep. Very useful to avoid conflicts in
multicore systems, must be avoided in single processor systems (use
mutexes instead).

See [here](https://sysprog21.github.io/lkmpg/#spinlocks) for the example. 

Finally, for single operations on simple data (i.e. integers),
consider using atomic operations, see [here](https://sysprog21.github.io/lkmpg/#atomic-operations) for an example.


<a id="org657695e"></a>

# TP 3

We continue the work done in TP2, and we add the possibility to
block the tasks under certain circumstances. 

1.  If the channel is full and a task wants to write additional data,
    it is **blocked** (sleeps) until there is at least one byte
    available for writing.
2.  If the channel is empty and a task wants to read data from it, it
    is **blocked** (sleeps) until there is at least one byte available
    for reading.

Handling conflicts: Since several tasks can read/write at the same
time executing in parallel on different cores, it is necessary to
protect the data structures with mutexes or with spinlocks.

-   Use a single spinlock first for the whole device. Pay attention,
    you must release the spinlock before sleeping
-   Try to think about using a mutex. What does it change?

**Warning!!!** Programming with mutexes and spinlocks is **DIFFICULT**.

