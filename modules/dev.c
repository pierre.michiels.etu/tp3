/* 

 * chardev.c: Creates a read-only char device that says how many times 

 * you have read from the dev file 

 */ 

 

#include <linux/cdev.h> 

#include <linux/delay.h> 

#include <linux/device.h> 

#include <linux/fs.h> 

#include <linux/init.h> 

#include <linux/irq.h> 

#include <linux/kernel.h> 

#include <linux/module.h> 

#include <linux/poll.h> 
#include <linux/slab.h>
#include <linux/spinlock.h> 
#include <linux/string.h> 

#include "dev.h"

 

/*  Prototypes - this would normally go in a .h file */ 

static int device_open(struct inode *, struct file *); 

static int device_release(struct inode *, struct file *); 

static ssize_t device_read(struct file *, char __user *, size_t, loff_t *); 

static ssize_t device_write(struct file *, const char __user *, size_t, 

                            loff_t *); 

 

#define SUCCESS 0 

#define DEVICE_NAME "chardev" /* Dev name as it appears in /proc/devices   */ 

#define BUF_LEN 16 /* Max length of the message from the device */ 

 

/* Global variables are declared as static, so are global within the file. */ 
static int major; /* major number assigned to our device driver */ 

/* Is device open? Used to prevent multiple access to device */ 
// static atomic_t already_open = ATOMIC_INIT(0); 
 
static atomic_t already_read = ATOMIC_INIT(0); 
atomic_t already_write = ATOMIC_INIT(0); 

/* Queue of processes who want our file */ 
static DECLARE_WAIT_QUEUE_HEAD(waitq_read); 
DECLARE_WAIT_QUEUE_HEAD(waitq_write);

DEFINE_SPINLOCK(sl_static); 
// static spinlock_t sl_dynamic; 

msg_t msg;
static char *msg_ptr; 

static struct class *cls; 

static struct file_operations chardev_fops = { 
    .read = device_read, 
    .write = device_write, 
    .open = device_open,  
    .release = device_release, 
}; 

 

int chardev_init(void)
{ 
    msg.size = BUF_LEN; 
    msg.first_write = 0;
    msg.first_read  = 0; 
    msg.can_read = 1;
    msg.text = kmalloc(msg.size, GFP_KERNEL);
    memset(msg.text, '\0', msg.size);
    major = register_chrdev(0, DEVICE_NAME, &chardev_fops); 
    if (major < 0) { 
        pr_alert("Registering char device failed with %d\n", major); 
        return major; 
    } 

    pr_info("I was assigned major number %d.\n", major); 

    cls = class_create(THIS_MODULE, DEVICE_NAME); 

    device_create(cls, NULL, MKDEV(major, 0), NULL, DEVICE_NAME); 

    pr_info("Device created on /dev/%s\n", DEVICE_NAME);
    return SUCCESS; 

} 

 

void chardev_exit(void) 

{ 
    kfree(msg.text);
    device_destroy(cls, MKDEV(major, 0)); 
    class_destroy(cls); 
 

    /* Unregister the device */ 
    unregister_chrdev(major, DEVICE_NAME); 

} 

 

/* Methods */ 

 

/* Called when a process tries to open the device file, like 
 * "sudo cat /dev/chardev" 
 */ 

static int device_open(struct inode *inode, struct file *file) 

{ 
    // if (atomic_cmpxchg(&already_open, 0, 1)) {
    //     return -EBUSY; 
    // }
        
    // sprintf(msg, "I already told you %d times Hello world!\n", counter++); 
    msg_ptr = msg.text;
    try_module_get(THIS_MODULE); 

    return SUCCESS; 
} 

 

/* Called when a process closes the device file. */ 
static int device_release(struct inode *inode, struct file *file) 

{ extern msg_t msg;
    /* atomic_set(&already_open, 0); */

    /* Decrement the usage count, or else once you opened the file, you will 

     * never get get rid of the module. 

     */ 
    module_put(THIS_MODULE); 

    return SUCCESS;
} 

unsigned long flags; 
static ssize_t device_read(struct file *filp, /* see include/linux/fs.h   */ 
                           char __user *buffer, /* buffer to fill with data */ 
                           size_t length, /* length of the buffer     */ 
                           loff_t *offset) 
{                                                                                                                                      

    /* Number of bytes actually written to the buffer */ 
    int bytes_read = 0; 

    /* If we are at the end of message, return 0 signifying end of file. */ 
    if (!msg.can_read) {
        msg.can_read = 1;
        return 0;
    }
   
    spin_lock_irqsave(&sl_static, flags);
    pr_info("==> Locked 1 static spinlock read, %d %d\n", *msg_ptr, msg.first_read); 

    while (msg.first_read == msg.first_write) {
        int i, is_sig = 0;
        
        atomic_set(&already_read, 1);
        spin_unlock_irqrestore(&sl_static, flags); 
        pr_info("Unlocked 1 static spinlock read\n"); 
        wait_event_interruptible(waitq_read, !atomic_read(&already_read));

        /* If we woke up because we got a signal we're not blocking,
         * return -EINTR (fail the system call).  This allows processes
         * to be killed or stopped.
         */
        for (i = 0; i < _NSIG_WORDS && !is_sig; i++)
            is_sig = current->pending.signal.sig[i] & ~current->blocked.sig[i];

        if (is_sig) {
            return -EINTR;
        }
        
        spin_lock_irqsave(&sl_static, flags);
        pr_info("Locked 2 static spinlock read\n"); 
    }
    // msg_ptr = msg.text;
    /* Actually put the data into the buffer */ 
    while (msg.first_read < msg.first_write) { 

        /* The buffer is in the user data segment, not the kernel 
         * segment so "*" assignment won't work.  We have to use 
         * put_user which copies data from the kernel data segment to 
         * the user data segment. 
         */ 
        put_user(*(msg_ptr+((msg.first_read++) % msg.size)), buffer++); 
        bytes_read++; 
    } 
    put_user(*(msg_ptr+((msg.first_read) % msg.size)), "\0"); 
    
    atomic_set(&can_reduce, 0);
    wake_up(&waitq_reduce);

    atomic_set(&already_write, 0);
    wake_up(&waitq_write);

    msg.can_read = 0;
    spin_unlock_irqrestore(&sl_static, flags); 
    pr_info("Unlocked 2 static spinlock read %d\n", bytes_read); 
    /* Most read functions return the number of bytes put into the buffer. */ 
    return bytes_read; 
}

 

/* Called when a process writes to dev file: echo "hi" > /dev/hello */ 

static ssize_t device_write(struct file *filp, const char __user *buff, 
                            size_t len, loff_t *off) 

{ 
    spin_lock_irqsave(&sl_static, flags);
    pr_info("Locked 1 static spinlock write %d %d\n", msg.first_write, msg.first_read); 

    int diff, i;

    for (i = 0; i < len - 1; i++) {
        if (msg.first_write - msg.first_read == msg.size) {
            int i, is_sig = 0;
            
            atomic_set(&already_read, 0);
            wake_up(&waitq_read);

            atomic_set(&already_write, 1);
            spin_unlock_irqrestore(&sl_static, flags); 
            pr_info("Unlocked 1 static spinlock write %d %d\n", msg.first_write, msg.first_read); 
            wait_event_interruptible(waitq_write, !atomic_read(&already_write));

            for (i = 0; i < _NSIG_WORDS && !is_sig; i++)
                is_sig = current->pending.signal.sig[i] & ~current->blocked.sig[i];

            if (is_sig) {
                return -EINTR;
            }
            
            spin_lock_irqsave(&sl_static, flags);
            pr_info("Locked 2 static spinlock write %d %d\n", msg.first_write, msg.first_read); 
        }

        pr_info("===> %d\n", i);
        msg.text[(msg.first_write++) % msg.size] = *(buff++);
        pr_info("%c\n", msg.text[msg.first_write-1 % 16]);
        // bytes_write++;
    }
    
    diff = msg.first_write - msg.first_read;
    if (diff > msg.size) {
        msg.first_write = msg.first_read + (diff % msg.size);
    }

    // msg.last_bytes_write += bytes_write;
    // total_bytes_write = msg.last_bytes_write;
    // if (msg.last_bytes_write == len)
    //     msg.last_bytes_write = 0;

    atomic_set(&already_read, 0);
    wake_up(&waitq_read);

    pr_info("=> %s %d\n", msg.text, strlen(msg.text));
    spin_unlock_irqrestore(&sl_static, flags); 
    pr_info("Unlocked 2 static spinlock write %d %d\n", msg.first_write, msg.first_read); 

    return len; 
}

// module_init(chardev_init); 
// module_exit(chardev_exit); 

MODULE_LICENSE("GPL");