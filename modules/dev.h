#ifndef BUFFER_H
#define BUFFER_H

typedef struct{
    char * text;
    int size;
    int first_write;
    int first_read;
    int can_read;
}msg_t;

extern msg_t msg;
extern spinlock_t sl_static;
extern atomic_t can_reduce;
extern atomic_t already_write;

extern struct wait_queue_head waitq_write;
extern struct wait_queue_head waitq_reduce;

extern  unsigned long flags; 

int chardev_init(void);
void chardev_exit(void);


#endif
