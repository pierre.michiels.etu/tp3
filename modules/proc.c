/* 
 * proc-hello.c 
 */ 

#include <linux/kernel.h> 
#include <linux/module.h> 
#include <linux/proc_fs.h> 
#include <linux/uaccess.h> 
#include <linux/version.h> 
#include <linux/poll.h> 
#include <linux/slab.h>
#include <linux/spinlock.h> 

#include "dev.h"

#if LINUX_VERSION_CODE >= KERNEL_VERSION(5, 6, 0) 
#define HAVE_PROC_OPS 
#endif 

static int procfile_open(struct inode *inode, struct file *file);
static int procfile_release(struct inode *inode, struct file *file);
static ssize_t procfile_read(struct file *filePointer, char __user *buffer, 
                             size_t buffer_length, loff_t *offset);
static ssize_t procfile_write(struct file *filp, const char __user *buff, 
                            size_t len, loff_t *off);


#define procfs_name "asee_channel" 
#define MSG_SIZE 128
#define PROCFS_MAX_SIZE 1024

atomic_t can_reduce = ATOMIC_INIT(0); 
DECLARE_WAIT_QUEUE_HEAD(waitq_reduce); 

/* The buffer used to store character for this module */ 

static char procfs_buffer[PROCFS_MAX_SIZE]; 

 

/* The size of the buffer */ 

static unsigned long procfs_buffer_size = 0; 

/* Is device open? Used to prevent multiple access to device */ 
static atomic_t already_open = ATOMIC_INIT(0); 

static struct proc_dir_entry *our_proc_file; 


/* Called when a process tries to open the procfile file, like 
 * "sudo cat /dev/chardev" 
 */ 

static int procfile_open(struct inode *inode, struct file *file) 

{ 
    // if (atomic_cmpxchg(&already_open, 0, 1)) {
    //     return -EBUSY; 
    // }
        
    // sprintf(msg, "I already told you %d times Hello world!\n", counter++); 
    try_module_get(THIS_MODULE); 

    return 0; 
} 

/* Called when a process closes the procfile file. */ 
static int procfile_release(struct inode *inode, struct file *file) 

{ 
    // atomic_set(&already_open, 0); /* We're now ready for our next caller */ 

    /* Decrement the usage count, or else once you opened the file, you will 

     * never get get rid of&sl_static the module. 

     */ 
    module_put(THIS_MODULE); 

    return 0;
} 


static ssize_t procfile_read(struct file *filePointer, char __user *buffer, 
                             size_t buffer_length, loff_t *offset) 
{ 
    // char s[13] = "HelloWorld!\n"; 
    // int len = sizeof(msg.size); 

    char s[MSG_SIZE]; 
    int len;
    ssize_t ret;
    snprintf(s, MSG_SIZE, "BUFFER_SIZE=%d, BUFFER_LEN=%d, FIRST_READ=%d, FIRST_WRITE=%d\n", msg.size, msg.first_write - msg.first_read, msg.first_read, msg.first_write);

    len = strlen(s); 

    ret = len; 

    if (*offset >= len || copy_to_user(buffer, s, len)) { 
        pr_info("copy_to_user failed\n"); 
        ret = 0; 
    } else { 
        pr_info("procfile read %s\n", filePointer->f_path.dentry->d_name.name); 
        *offset += len; 
    } 

    return ret; 
 
}

static ssize_t procfile_write(struct file *filp, const char __user *buff, 
                            size_t len, loff_t *off) 

{ 
    
    long size;
    int i;
    char *new_msg;

    procfs_buffer_size = len; 

    if (procfs_buffer_size > PROCFS_MAX_SIZE) 
        procfs_buffer_size = PROCFS_MAX_SIZE; 

    if (copy_from_user(procfs_buffer, buff, procfs_buffer_size)) 
        return -EFAULT; 
    procfs_buffer[procfs_buffer_size] = '\0';

    if (kstrtol(procfs_buffer, 10, &size)) {
        pr_info("here %d\n", size); 
        return -EINVAL;
    }
    spin_lock_irqsave(&sl_static, flags);
    pr_info("==> Locked 1 static spinlock proc write\n"); 

    while (size < msg.first_write - msg.first_read) {
        int i, is_sig = 0;
        
        atomic_set(&can_reduce, 1);
        spin_unlock_irqrestore(&sl_static, flags); 
        pr_info("Unlocked 1 static spinlock read\n"); 
        wait_event_interruptible(waitq_reduce, !atomic_read(&can_reduce));

        for (i = 0; i < _NSIG_WORDS && !is_sig; i++)
            is_sig = current->pending.signal.sig[i] & ~current->blocked.sig[i];

        if (is_sig) {
            return -EINTR;
        }
        
        spin_lock_irqsave(&sl_static, flags);
        pr_info("Locked 2 static spinlock proc read\n"); 
    }

    
    new_msg = kmalloc(size, GFP_KERNEL);
    
    for (i = msg.first_read; i < msg.first_write; i++) 
        new_msg[i-msg.first_read] = msg.text[i%msg.size];
    
    kfree(msg.text);

    msg.text = new_msg;
    msg.size = size;
    msg.first_read = 0;
    msg.first_write = i;

    atomic_set(&already_write, 0);
    wake_up(&waitq_write);

    spin_unlock_irqrestore(&sl_static, flags); 
    pr_info("Unlocked 2 static spinlock proc write\n");
    
    return len; 
}


#ifdef HAVE_PROC_OPS 
static const struct proc_ops proc_file_fops = { 
    .proc_read = procfile_read, 
    .proc_write = procfile_write,
    .proc_open = procfile_open,
    .proc_release = procfile_release,
}; 
#else 
static const struct file_operations proc_file_fops = { 
    .read = procfile_read, 
    .write = procfile_write,
    .open = procfile_open,
    .release = procfile_release,
}; 
#endif 

static int procfs1_init(void) 
{ 
    our_proc_file = proc_create(procfs_name, 0644, NULL, &proc_file_fops); 
    if (NULL == our_proc_file) { 
        proc_remove(our_proc_file); 
        pr_alert("Error:Could not initialize /proc/%s\n", procfs_name); 
        return -ENOMEM; 
    } 
    pr_info("/proc/%s created\n", procfs_name); 
    return 0; 
} 

static void procfs1_exit(void) 
{ 
    proc_remove(our_proc_file); 
    pr_info("/proc/%s removed\n", procfs_name); 
}

static int init_wrapper(void) {
    chardev_init();
    procfs1_init();
    return 0;
}

static void exit_wrapper(void) {
    chardev_exit();
    procfs1_exit();
}

module_init(init_wrapper); 
module_exit(exit_wrapper);


MODULE_LICENSE("GPL");
