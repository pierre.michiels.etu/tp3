#!/bin/bash
KERNEL=${1-/home/baht/Master/ASEE/tp2/build/kvm/arch/x86/boot/bzImage}
SMP=${2-2}

qemu-system-x86_64 -enable-kvm -smp $SMP -m 1G -boot c \
    -kernel $KERNEL -append "root=/dev/sda1 console=ttyS0 rw" \
    -hda debian.qcow \
    -net nic -net user,hostfwd=tcp::10022-:22 \
    -serial mon:stdio

