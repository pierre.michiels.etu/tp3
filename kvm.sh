SMP=${2-2}

qemu-system-x86_64 -enable-kvm -smp $SMP -m 2048 -boot c \
	-hda debian.qcow \
	-net nic -net user,hostfwd=tcp::10022-:22 \
	-serial stdio

